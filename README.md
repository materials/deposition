# deposition
This is a repository (v1.0) of the paper

**Christoph Scherer, Naomi Kinaret, Kun-Han Lin, Muhammad Nawaz Qaisrani, Felix Post, Falk May, and Denis Andrienko**, 
Predicting molecular ordering in deposited molecular films

In the every folder there is an all-atom (aa.ff) and coarse-grained (cg.ff) force-fields. 

deposition.ipynb is a python notebook that summarizes the rate model

